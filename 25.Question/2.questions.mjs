let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a1",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a2",
    },
  },
];

//find the array of id ie  output must be [1,2,3]
let ids = products.map((value, i) => {
  return value.id;
});
//console.log(ids);

//find the array of title ie output must be ["Product 1", "Product 2", "Product 3"]
let titles = products.map((value, i) => {
  return value.title;
});
//console.log(titles);

let categorys = products.map((value, i) => {
  return value.category;
});
//console.log(categorys);
console.log();

let types = products.map((value, i) => {
  return value.discount.type;
});
console.log(types);
console.log();

//find the array of price where each price is multiplied by 3  output must be [ 15000,6000,9000]
//important
// map is used to modify elements of input
// filter is used to filter the elements of input

let price = products.map((value, i) => {
  return value.price * 3;
});
console.log(price);
console.log();
console.log();
