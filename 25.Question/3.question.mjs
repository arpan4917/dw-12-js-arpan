let products = [
    {
      id: 1,
      title: "Product 1",
      category: "electronics",
      price: 5000,
      description: "This is description and Product 1",
      discount: {
        type: "other",
      },
    },
    {
      id: 2,
      title: "Product 2",
      category: "cloths",
      price: 2000,
      description: "This is description and Product 2",
      discount: {
        type: "a1",
      },
    },
    {
      id: 3,
      title: "Product 3",
      category: "electronics",
      price: 3000,
      description: "This is description and Product 3",
      discount: {
        type: "a2",
      },
    },
  ];
//find those array  whose price is >= 3000 =>  
  let price3000 = products.filter ((value,i) => {
    if (value.price>=3000) return true;

  });
  //console.log(prices);
  //console.log();
  // if there is only one code you do not need to write curly braces and vice versa

  //find those array of  title whose price is >= 3000=>["product 1",product 3]

  let product3000 = products.filter((value,i) =>{
    if (value.price>=3000) {
        return true;
    }
  })
  .map((value,i)=>{
    return value.title;
  });
//   console.log(product3000);
//   console.log();

  //find those array of title whose price does not equal to 5000 ==> ["product 2","product 3"]
let product5000 = products.filter((value,i)=> {
    if (value.price!==5000) {
        return true;
    }
})
.map((value,i)=>{
    return value.title;
});
//console.log(product5000);
//console.log();

// find those array of category whose price ====>3000 "electronics"

let category3000 = products.filter((value,i)=>{
    if(value.price===3000){
        return true;
    }
})
.map((value,i)=>{
    return value.category;
});
console.log(category3000);

  
